package com.example.boatpilot

import android.app.Activity
import android.os.Handler
import android.util.Log
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import okhttp3.*
import java.util.concurrent.TimeUnit

inline fun <reified T> Any?.tryCast(block: T.() -> Unit) {
    if (this is T) {
        block()
    }
}

class ServerConnection(val activity: Activity): WebSocketListener() {
    private val TAG = "ServerConnection"

    private lateinit var socket: WebSocket

    private val parser = Parser.default()
    private var lastUpdate = 0L

    init {
        instance = this
    }

    companion object {
        const val SWITCH_BANK = 5

        val heading = MutableLiveData<Number>()
        val autopilotHeading = MutableLiveData<Number>()
        val targetHeading = MutableLiveData<Number?>()
        val rudder = MutableLiveData<Number>()
        val depth = MutableLiveData<Number>()
        val seaTemperature = MutableLiveData<Number>()
        val knots = MutableLiveData<Number>()
        val voltage = MutableLiveData<Number>()
        val locationDecimal = MutableLiveData<String>()
        val locationCoord = MutableLiveData<String>()
        val charge = MutableLiveData<Number>()
        val amps = MutableLiveData<Number>()
        val pitch = MutableLiveData<Number>()
        val roll = MutableLiveData<Number>()
        val autopilotStatus = MutableLiveData<JsonObject>()
        val autopilotStandby = MutableLiveData<Boolean>()
        val autopilotTrackControl = MutableLiveData<JsonObject>()

        lateinit var instance: ServerConnection

        fun sendIntent(intent: String) {
            instance.sendMessage(JsonObject(mapOf(
                "channel" to "intent",
                "data" to intent
            )))
        }

        fun sendPGN(data: JsonObject) {
            instance.sendMessage(JsonObject(mapOf(
                "channel" to "nmea.send",
                "data" to data
            )))
        }

        fun toggleSwitch(name: String, value: Boolean) {
            sendPGN(JsonObject(mapOf(
                "pgn" to 127502,
                "dst" to 255,
                "fields" to mapOf(
                    "Instance" to SWITCH_BANK,
                    name to if (value) "On" else "Off"
                ))
            ))
        }
    }

    init {
        autopilotStatus.observe(activity as LifecycleOwner) {
            val indicator1 = it.get("Indicator1")
            autopilotStandby.postValue(indicator1 == "On")
        }

        autopilotTrackControl.observe(activity as LifecycleOwner) {
            val target = it.get("Heading-To-Steer (Course)") as Number?
            targetHeading.postValue(if (target != null) rad2Deg(target.toDouble()) else null)

            val pilotHeading = it.get("Vessel Heading") as Number?
            autopilotHeading.postValue(if (pilotHeading != null) rad2Deg(pilotHeading.toDouble()) else 0.0)
        }
    }

    private fun rad2Deg(number: Double): Double {
        return number * 57.2957795131
    }

    private class ValueUpdater<T>(key: String, val value: MutableLiveData<T>, val forceNull: Boolean = false) {
        val keyList = key.split(".")

        fun update(data: JsonObject) {
            var current: JsonObject? = data

            val count = keyList.count()
            for ( k in 0..count ) {
                val v = current?.get(keyList[k])
                if ( k == count - 1 ) {
                    value.postValue(if (forceNull) null else v as T)
                    return
                }

                current = v as JsonObject?
            }
        }
    }

    private val updaters = listOf<Pair<String, ValueUpdater<*>>>(
        "sensor.heading" to ValueUpdater("data.heading", heading),
        "sensor.rudder" to ValueUpdater("data.angle", rudder),
        "sensor.depth" to ValueUpdater("data.depth", depth),
        "sensor.temperature-sea" to ValueUpdater("data.temp", seaTemperature),
        "sensor.speed" to ValueUpdater("data.knots", knots),
        "sensor.battery" to ValueUpdater("data.charge", charge),
        "sensor.location" to ValueUpdater("data.decimal.string", locationDecimal),
        "sensor.location" to ValueUpdater("data.coord.string", locationCoord),
        "sensor.battery" to ValueUpdater("data.current", amps),
        "sensor.battery" to ValueUpdater("data.voltage", voltage),
        "sensor.attitude" to ValueUpdater("data.pitch", pitch),
        "sensor.attitude" to ValueUpdater("data.roll", roll),
        "nmea.127501" to ValueUpdater("fields", autopilotStatus),
        "nmea.127237" to ValueUpdater("fields", autopilotTrackControl)
    )

    fun connect() {
        val channels = updaters.fold(mutableSetOf<String>()) { acc, cur ->
            acc.add(cur.first)
            acc
        }.joinToString(",")

        val request = Request.Builder()
            .url("ws://10.0.0.2:8081/connect?channels=${channels}")
            .build()

        val client = OkHttpClient.Builder()
            .readTimeout(0, TimeUnit.MILLISECONDS)
            .build()

        socket = client.newWebSocket(request, this)
    }

    fun close() {
        socket.close(1000, "Socket close")
    }

    override fun onOpen(webSocket: WebSocket, response: Response) {
        super.onOpen(webSocket, response)
        Log.d(TAG, "onOpen:")

        /**
         * Immediately request status of Autopilot
         * using ISO request PGN 59904
         */
        sendPGN(JsonObject(mapOf(
            "pgn" to 59904,
            "dst" to 255,
            "fields" to mapOf(
                "PGN" to 127501
            )
        )))
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        super.onMessage(webSocket, text)

        val stringBuilder = StringBuilder(text)
        val json = parser.parse(stringBuilder) as JsonObject

        val channel = json["channel"] as String
        val data = json["data"] as JsonObject

        updaters.filter { it.first == channel }
                .forEach {
                    it.second.update(data)
                }
    }

    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
        super.onClosing(webSocket, code, reason)
        Log.d(TAG, "onClosing: $code $reason")
    }

    override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
        super.onClosed(webSocket, code, reason)
        Log.d(TAG, "onClosed: $code $reason")
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        Log.d(TAG, "onFailure: ${t.message} $response")
        super.onFailure(webSocket, t, response)

        Handler(activity.mainLooper).postDelayed(::connect, 1000)
    }

    fun sendMessage(obj: JsonObject) {
        socket?.send(obj.toJsonString())
    }
}