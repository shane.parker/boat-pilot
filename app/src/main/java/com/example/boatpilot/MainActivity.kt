package com.example.boatpilot

import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.boatpilot.databinding.ActivityMainBinding
import com.example.boatpilot.ui.views.*


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var connection: ServerConnection
    private lateinit var locationManager: LocationManager
    private lateinit var dataLayout: LinearLayout

    private fun addDataView(name: String, view: (Context, Boolean) -> ValueView<*>) {
        dataLayout.addView(DataView(applicationContext, name, view(applicationContext, false), false))
    }

    private fun addDataView(name: String, views: Map<String, (Context, Boolean) -> ValueView<*>>) {
        val group = LinearLayout(applicationContext)
        group.orientation = LinearLayout.HORIZONTAL

        views.entries.forEach {
            group.addView(DataView(
                applicationContext,
                it.key,
                it.value(applicationContext, true),
                (name !== ""))
            )
        }

        if ( name !== "" ) {
            dataLayout.addView(DataView(applicationContext, name, group, false))
        } else {
            dataLayout.addView(group)
        }
    }

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        window.decorView.windowInsetsController?.hide(
            android.view.WindowInsets.Type.statusBars()
        )

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        dataLayout = findViewById(R.id.dataLayout)

        connection = ServerConnection(this)

        addDataView("", mapOf(
            "Knots" to ::KnotsView,
            "Depth" to ::DepthView,
            "Sea Temp" to ::SeaTempView
        ))

        addDataView("Location", ::LocationView)
        addDataView("Battery", mapOf(
            "Voltage" to ::VoltageView,
            "Amps" to ::AmpsView,
            "Charge" to ::ChargeView
        ))

        val toggleAutopilotButton = findViewById<Button>(R.id.toggleAutopilotButton)
        toggleAutopilotButton.setOnClickListener {
            ServerConnection.toggleSwitch(
                if (ServerConnection.autopilotStandby.value == true) "Switch2" else "Switch1", true)
        }

        ServerConnection.autopilotStandby.observe(this) {
            toggleAutopilotButton.setText(
                if (!it) R.string.standby else R.string.auto
            )
        }

        connection.connect()
    }

    override fun onDestroy() {
        super.onDestroy()
        connection.close()
    }

    fun updateLocation(latitude: Double, longitude: Double) {
        val mockLocation = Location(LocationManager.GPS_PROVIDER)
        mockLocation.latitude = latitude
        mockLocation.longitude = longitude
        mockLocation.altitude = 0.0
        mockLocation.time = System.currentTimeMillis()
        mockLocation.accuracy = 1.0f
        mockLocation.elapsedRealtimeNanos = SystemClock.elapsedRealtimeNanos()
        locationManager.setTestProviderLocation(LocationManager.GPS_PROVIDER, mockLocation)
    }
}