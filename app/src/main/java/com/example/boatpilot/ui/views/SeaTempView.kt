package com.example.boatpilot.ui.views

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import com.example.boatpilot.ServerConnection

class SeaTempView(context: Context, small: Boolean): ValueView<Number>(
    context,
    ServerConnection.seaTemperature,
    small,
    {
        val f = (it.toFloat() - 273.15f) * 9.0f / 5.0f + 32.0f
        "%.${1}f".format(f)
    })
