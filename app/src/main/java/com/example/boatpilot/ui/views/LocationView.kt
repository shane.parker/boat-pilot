package com.example.boatpilot.ui.views

import android.content.Context
import com.example.boatpilot.ServerConnection

class LocationView(context: Context, small: Boolean): ValueView<String>(
    context,
    ServerConnection.locationDecimal,
    true,
    { it })
