package com.example.boatpilot.ui.views

import android.content.Context
import com.example.boatpilot.ServerConnection

class DepthView(context: Context, small: Boolean): ValueView<Number>(
    context,
    ServerConnection.depth,
    small,
    {
        val ft = it.toFloat() * 3.28084f
        if ( ft < 6.0 ) {

        }

        "%.${1}f".format(it.toFloat() * 3.28084f)
    })
