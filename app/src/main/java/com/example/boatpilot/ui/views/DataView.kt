package com.example.boatpilot.ui.views

import android.content.Context
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import com.example.boatpilot.R

open class DataView(
    context: Context,
    name: String,
    view: View,
    small: Boolean,
    layoutId: Int = R.layout.data_layout): FrameLayout(context) {
    init {
        inflate(getContext(), layoutId, this)

        val nameLabel = findViewById<TextView>(R.id.dataName)
        nameLabel?.text = name
        nameLabel.textSize = resources.getDimension(
            if (small) R.dimen.subHeaderSize else R.dimen.headerSize)

        val frame = findViewById<FrameLayout>(R.id.dataFrame)
        frame?.addView(view)

        layoutParams = LinearLayout.LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT,
            1.0f
        )
    }
}
