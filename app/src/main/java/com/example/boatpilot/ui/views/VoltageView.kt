package com.example.boatpilot.ui.views

import android.content.Context
import com.example.boatpilot.ServerConnection

class VoltageView(context: Context, small: Boolean): ValueView<Number>(
    context,
    ServerConnection.voltage,
    small,
    { "%.${2}f".format(it.toFloat()) })
