package com.example.boatpilot.ui.views

import android.content.Context
import com.example.boatpilot.ServerConnection

class HeadingView(context: Context, small: Boolean): ValueView<Number>(
    context,
    ServerConnection.autopilotHeading,
    small,
    { "%.${1}f".format(it.toFloat()) })
