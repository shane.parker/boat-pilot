package com.example.boatpilot.ui.views

import android.content.Context
import android.graphics.*
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.lifecycle.findViewTreeLifecycleOwner
import com.example.boatpilot.R
import com.example.boatpilot.ServerConnection
import java.lang.Integer.min
import java.math.RoundingMode
import kotlin.math.PI
import kotlin.math.abs


class PilotView(context: Context, attrs: AttributeSet): View(context, attrs) {
    private val padding = 4.0f

    private data class CompassPoint(val angle: Int, val label: String, val scale: Float, val color: Int)

    private var guageBackgroundColor = 0
    private var primaryColor = 0
    private var highlightColor = 0
    private var headingColor = 0
    private var rudderColor = 0
    private var targetHeadingColor = 0
    private var shadowColor = 0

    private var headingWidth = 0.05f
    private var rudderWidth = headingWidth / 2.0f
    private var targetHeadingWidth = 0.1f
    private val headingLength = 1.0f
    private val rudderLength = 0.75f

    private val DRAW_THRESHOLD = 0.10f
    private val ANIMATE_FPS = 24

    private var radius = 0.0f

    private var lwidth1 = 0.0f
    private var pwidth1 = 0.0f
    private var pwidth2 = 0.0f
    private var pwidth3 = 0.0f

    private var pointTextSize = 1.0f

    private var drawHeadingAngle: Double? = null
    private var drawRudderAngle: Double? = null

    private val compassPoints = arrayOf(
        CompassPoint(0, "N", 1.55f, highlightColor),
        CompassPoint(90, "E", 1.55f, highlightColor),
        CompassPoint(180, "S", 1.55f, highlightColor),
        CompassPoint(270, "W", 1.55f, highlightColor)
    )

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.PilotView,
            0, 0).apply {

            try {
                guageBackgroundColor = getColor(R.styleable.PilotView_backgroundColor, Color.TRANSPARENT)
                primaryColor = getColor(R.styleable.PilotView_primaryColor, Color.WHITE)
                highlightColor = getColor(R.styleable.PilotView_highlightColor, Color.BLACK)
                headingColor = getColor(R.styleable.PilotView_headingColor, Color.BLACK)
                rudderColor = getColor(R.styleable.PilotView_rudderColor, Color.GRAY)
                targetHeadingColor = getColor(R.styleable.PilotView_targetHeadingColor, Color.GRAY)
                shadowColor = getColor(R.styleable.PilotView_shadowColor, Color.TRANSPARENT)
            } finally {
                recycle()
            }
        }
    }

    fun scaled(v: Float): Float { return v * radius }

    private var hasChanged = false

    private fun changed() {
        if ( (handler == null) || hasChanged ) {
            return
        }
        hasChanged = true

        handler.postDelayed({
            hasChanged = false

            drawHeadingAngle?.let {
                ServerConnection.autopilotHeading.value?.let { heading ->
                    val turnDir = shortestTurnDir(it, heading.toDouble())
                    if (abs(turnDir) >= DRAW_THRESHOLD) {
                        drawHeadingAngle = wrap(0.0, 360.0, it + turnDir)
                        changed()
                    }
                }
            }

            invalidate()
        }, (1000.0 / ANIMATE_FPS).toLong())
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        val lifecycleOwner = findViewTreeLifecycleOwner()
        lifecycleOwner?.let {
            ServerConnection.autopilotHeading.observe(it) {
                val dbl = it.toDouble().toBigDecimal().setScale(1, RoundingMode.DOWN).toDouble()
                if (drawHeadingAngle == null) {
                    drawHeadingAngle = dbl
                }

                changed()
            }

            ServerConnection.rudder.observe(it) {
                val dbl = it.toDouble()

                val dist = abs(dbl - (drawRudderAngle ?: 0.0))
                if ( (dist >= DRAW_THRESHOLD) || (drawRudderAngle == null) ) {
                    drawRudderAngle = dbl
                    changed()
                }
            }
        }
    }

    private fun shortestTurnDir(from: Double, to: Double): Double {
        return (((to - from) + 540.0) % 360.0) - 180.0
    }

    private fun wrap(min: Double, max: Double, x: Double): Double {
        if (x < min) {
            return max - (min - x) % (max - min)
        }

        return min + (x - min) % (max - min)
    }

    // Called when the view should render its content.
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        //Log.d("PilotView", "Drawing ${System.currentTimeMillis()}")

        radius = (min(width, height) / 2) - padding
        pointTextSize = scaled(0.12f)
        lwidth1 = scaled(0.0275f)
        pwidth1 = scaled(0.025f)
        pwidth2 = pwidth1 / 1.5f
        pwidth3 = pwidth2 / 4.0f

        centerTransform(canvas)
        drawBackground(canvas)

        ServerConnection.targetHeading.value?.let {
            drawAngle(canvas, it.toDouble())
        }

        drawRudderAngle?.let {
            canvas?.save()
            canvas?.rotate((-it + 180.0).toFloat())
            drawNeedle(canvas, -it, rudderColor, scaled(rudderWidth), rudderLength)
            canvas?.restore()
        }

        drawHeadingAngle?.let {
            drawNeedle(canvas, it.toDouble(), headingColor, scaled(headingWidth), headingLength)
        }

        drawCompass(canvas)
    }

    private fun drawNeedle(canvas: Canvas?, angle: Double, color: Int, width: Float, length: Float) {
        canvas?.drawPath(Path().apply {
            moveTo(0.0f, 0.0f )
            lineTo(-width, 0.0f)
            lineTo(0.0f, -radius*length)
            lineTo(width, 0.0f)
            addCircle(0.0f, 0.0f, width, Path.Direction.CW)
        }, Paint().apply {
            isAntiAlias = true
            style = Paint.Style.FILL
            setShadowLayer(2.0f, 0.0f, 1.0f, shadowColor)
            this.color = color
        })
    }


    private fun drawAngle(canvas: Canvas?, angle: Double) {
        drawHeadingAngle?.let {
            //val start = wrap(0.0, 360.0, it + angle)
            val dist = shortestTurnDir(it, angle)

            val start = if (dist > 0.0) 0.0 else wrap(0.0, 360.0, dist)
            val sweep = abs(dist)

            canvas?.save()
            canvas?.rotate(-90f)

            val width = scaled(targetHeadingWidth) / radius
            val mid = 1.0f - width

            canvas?.drawArc(
                RectF(-radius, -radius, radius, radius),
                start.toFloat(),
                sweep.toFloat(),
                true,
                Paint().apply {
                    shader = RadialGradient(
                        0.0f,
                        0.0f,
                        radius,
                        intArrayOf(Color.TRANSPARENT,
                            Color.TRANSPARENT, targetHeadingColor, targetHeadingColor),
                        floatArrayOf(0.0f, mid, mid, 1.0f),
                        Shader.TileMode.CLAMP
                    )
                    style = Paint.Style.FILL
                }
            )

            canvas?.restore()
        }
    }

    fun drawBackground(canvas: Canvas?) {
        canvas?.drawArc(
            RectF(-radius, -radius, radius, radius),
            0.0f,
            Math.toDegrees(2 * PI).toFloat(),
            true,
            Paint().apply {
                isAntiAlias = true
                color = guageBackgroundColor
                style = Paint.Style.FILL
            }
        )
    }

    fun centerTransform(canvas: Canvas?) {
        canvas?.translate(width / 2.0f, height / 2.0f)
    }

    fun drawCompass(canvas: Canvas?) {
        canvas?.save()

        drawHeadingAngle?.let{
            canvas?.rotate(-it.toFloat())

            canvas?.drawArc(
                RectF(-radius, -radius, radius, radius),
                0.0f,
                Math.toDegrees(2 * PI).toFloat(),
                true,
                Paint().apply {
                    isAntiAlias = true
                    color = primaryColor
                    style = Paint.Style.STROKE
                    strokeWidth = lwidth1
                }
            )

            drawPoints(canvas, pwidth1, 0.10f, 30, true)
            drawPoints(canvas, pwidth2, 0.05f, 10)
            drawPoints(canvas, pwidth3, 0.05f, 5)
        }

        canvas?.restore()
    }

    fun drawPoints(
        canvas: Canvas?,
        width: Float,
        length: Float,
        step: Int,
        label: Boolean = false,
        color: Int = primaryColor)
    {
        drawHeadingAngle?.let {
            for (x in 0 until 360 step step) {
                canvas?.save()
                canvas?.rotate(x.toFloat())

                val paint = Paint().apply {
                    isAntiAlias = true
                    style = Paint.Style.STROKE
                    strokeWidth = width
                    this.color = color
                }

                val y = -radius + scaled(length)
                canvas?.drawLine(0.0f, y, 0.0f, -radius, paint)

                if (label) {
                    val spaceY = scaled(0.02f)

                    val textPaint = Paint().apply {
                        isAntiAlias = true
                        isSubpixelText = true
                        style = Paint.Style.FILL
                        textAlign = Paint.Align.CENTER
                        textSize = pointTextSize
                        this.color = color
                    }

                    canvas?.drawText(
                        x.toString(),
                        0.0f,
                        y + (pointTextSize / 2) + (spaceY * 2),
                        textPaint
                    )

                    compassPoints.find { it.angle == x }?.let {
                        val (_, label, scale, color) = it
                        val sz = pointTextSize * scale

                        val textPaint = Paint().apply {
                            isAntiAlias = true
                            isSubpixelText = true
                            style = Paint.Style.FILL
                            textAlign = Paint.Align.CENTER
                            textSize = sz
                            this.color = color
                        }

                        canvas?.drawText(
                            label,
                            0.0f,
                            y + (pointTextSize / 2) + (sz / 2) + (spaceY * 5.5f),
                            textPaint
                        )
                    }
                }

                canvas?.restore()
            }
        }
    }
}
