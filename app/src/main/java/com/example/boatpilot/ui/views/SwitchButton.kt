package com.example.boatpilot.ui.views

import android.content.Context
import android.content.Context.VIBRATOR_SERVICE
import android.os.Build
import android.os.CombinedVibration
import android.os.VibrationEffect
import android.os.Vibrator
import android.os.VibratorManager
import android.util.AttributeSet
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewTreeLifecycleOwner
import com.example.boatpilot.R
import com.example.boatpilot.ServerConnection

class SwitchButton(context: Context, attrs: AttributeSet):
    androidx.appcompat.widget.AppCompatButton(context, attrs) {

    private val switchName: String
    private val disabledOnStandby: Boolean

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.SwitchButton,
            0, 0).apply {

            try {
                switchName = getString(R.styleable.SwitchButton_switchName) ?: ""
                disabledOnStandby = getBoolean(R.styleable.SwitchButton_disabledOnStandby, false)
            } finally {
                recycle()
            }
        }

        setOnClickListener {
            Log.d("SwitchButton", switchName)

            if ( switchName != "" ) {
                ServerConnection.toggleSwitch(switchName, true)
            }

            vibratePhone()
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        if ( disabledOnStandby ) {
            ViewTreeLifecycleOwner.get(this)?.let {
                ServerConnection.autopilotStandby.observe(it) {
                    alpha = if (it) 0.5f else 1.0f
                    isEnabled = !it
                }
            }
        }
    }

    private fun vibratePhone() {
        val vibration = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            val vbManager =
                context.getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager
            vbManager.defaultVibrator
        } else {
            @Suppress("DEPRECATION")
            context.getSystemService(VIBRATOR_SERVICE) as Vibrator
        }

        if (vibration.hasVibrator()) {
            vibration.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE))
        }
    }
}