package com.example.boatpilot.ui.views

import android.content.Context
import android.content.Context.VIBRATOR_SERVICE
import android.os.Build
import android.os.CombinedVibration
import android.os.VibrationEffect
import android.os.Vibrator
import android.os.VibratorManager
import android.util.AttributeSet
import android.util.Log
import androidx.annotation.RequiresApi
import com.example.boatpilot.R
import com.example.boatpilot.ServerConnection

class IntentButton(context: Context, attrs: AttributeSet):
    androidx.appcompat.widget.AppCompatButton(context, attrs) {

    private val intent: String

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.IntentButton,
            0, 0).apply {

            try {
                intent = getString(R.styleable.IntentButton_intent) ?: ""
            } finally {
                recycle()
            }
        }

        setOnClickListener {
            Log.d("IntentButton", intent)

            ServerConnection.sendIntent(intent)
            vibratePhone()
        }
    }

    private fun vibratePhone() {
        val vibration = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            val vbManager =
                context.getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager
            vbManager.defaultVibrator
        } else {
            @Suppress("DEPRECATION")
            context.getSystemService(VIBRATOR_SERVICE) as Vibrator
        }

        if (vibration.hasVibrator()) {
            vibration.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE))
        }
    }
}