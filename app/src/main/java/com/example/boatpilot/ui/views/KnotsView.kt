package com.example.boatpilot.ui.views

import android.content.Context
import com.example.boatpilot.ServerConnection

class KnotsView(context: Context, small: Boolean): ValueView<Number?>(
    context,
    ServerConnection.knots,
    small,
    { "%.${1}f".format(it?.toFloat()) })
