package com.example.boatpilot.ui.views

import android.content.Context
import com.example.boatpilot.ServerConnection

class ChargeView(context: Context, small: Boolean): ValueView<Number>(
    context,
    ServerConnection.charge,
    small,
    { "%d%%".format(it.toInt()) })
