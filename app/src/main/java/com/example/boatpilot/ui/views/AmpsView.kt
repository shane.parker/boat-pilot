package com.example.boatpilot.ui.views

import android.content.Context
import com.example.boatpilot.ServerConnection

class AmpsView(context: Context, small: Boolean): ValueView<Number>(
    context,
    ServerConnection.amps,
    small,
    {
        val fl = it.toFloat()
        "${if (fl >= 0) "+" else ""}${"%.${2}f".format(fl)}"
    })
