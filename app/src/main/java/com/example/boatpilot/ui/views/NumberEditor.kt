package com.example.boatpilot.ui.views

import com.example.boatpilot.R
import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.annotation.Nullable

class NumberEditor(context: Context?, attrs: AttributeSet?) :
    LinearLayout(context, attrs) {
    private val et_number: EditText?

    var step = 1
    var min = 0
    var max = 10

    init {
        inflate(context, R.layout.number_editor, this)
        et_number = findViewById(R.id.et_number)
        val btn_less: Button = findViewById(R.id.btn_less)
        btn_less.setOnClickListener(AddHandler(-step))
        val btn_more: Button = findViewById(R.id.btn_more)
        btn_more.setOnClickListener(AddHandler(step))
    }

    /***
     * HANDLERS
     */
    private inner class AddHandler(val diff: Int) : OnClickListener {
        override fun onClick(v: View?) {
            var newValue = value + diff
            if (newValue < min) {
                newValue = min
            } else if (newValue > max) {
                newValue = max
            }
            et_number!!.setText(newValue.toString())
        }
    }

    /***
     * GETTERS & SETTERS
     */
    var value: Int
        get() {
            if (et_number != null) {
                try {
                    val value = et_number.text.toString()
                    return value.toInt()
                } catch (ex: NumberFormatException) {
                    Log.e("HorizontalNumberPicker", ex.toString())
                }
            }
            return 0
        }
        set(value) {
            et_number?.setText(value.toString())
        }
}