package com.example.boatpilot.ui.views

import android.content.Context
import android.util.Log
import android.widget.FrameLayout
import android.widget.TextView
import androidx.annotation.Dimension
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewTreeLifecycleOwner
import androidx.lifecycle.findViewTreeLifecycleOwner
import com.example.boatpilot.R

open class ValueView<T>(
    context: Context,
    private val value: LiveData<T>,
    val small: Boolean,
    private val formatter: ((T) -> String)? = null,
    layoutId: Int = R.layout.value_layout): FrameLayout(context) {

    init {
        inflate(getContext(), layoutId, this)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        val valueLabel = findViewById<TextView>(R.id.valueLabel)

        valueLabel.textSize = resources.getDimension(
            if (small) R.dimen.subValueSize else R.dimen.valueSize)

        val lifecycleOwner = findViewTreeLifecycleOwner()
        lifecycleOwner?.let {
            value.observe(it) {
                try {
                    valueLabel?.text = formatter?.invoke(it) ?: it.toString()
                } catch ( err: Exception ) {
                    Log.d("ValueView", "Error updating value view: $err")
                }
            }
        }
    }
}
